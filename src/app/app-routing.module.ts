import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path:'accueil',
    component: AccueilComponent
  },
  {
    path: 'user',
    component: UserComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

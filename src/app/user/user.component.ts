import { AfterContentInit, Component } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements AfterContentInit {
  ngAfterContentInit(){
    // alert('html charge');
  }
  public coyote: Array<{ name: string, age: number, category: string }> = [
    { name: "shazam", age: 19, category: "adult" },
    { name: "kevin", age: 16, category: "teenager" },
    { name: "melvin", age: 12, category: "teenager" }
  ];

  // getObjectKeys(obj: any): string[] {
  //   return Object.keys(obj);
  // }

}

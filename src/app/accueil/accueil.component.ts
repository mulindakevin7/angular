import { Component } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent {
  public datas : Array <string|number>= ["titi", 2,"tutu"];

  // cree un evement(event)
  log(event:any): void {
    console.log('toto', event);
    event.target.outerHTML = "<p>TOTO</p>"
  }
}
